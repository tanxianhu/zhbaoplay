
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.Html] {

    /**/
    def apply():play.api.templates.Html = {
        _display_ {

Seq[Any](format.raw/*1.1*/("""<!doctype html>
<html lang="zh_CN">
<head>
    <meta charset="UTF-8">
    <title>Backbone Study</title>
  <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*6.48*/routes/*6.54*/.Assets.at("stylesheets/bootstrap.css"))),format.raw/*6.93*/("""">
  <style type="text/css">
      body """),format.raw/*8.12*/("""{"""),format.raw/*8.13*/("""
        padding-top: 60px;
        padding-bottom: 40px;
      """),format.raw/*11.7*/("""}"""),format.raw/*11.8*/("""
      .sidebar-nav """),format.raw/*12.20*/("""{"""),format.raw/*12.21*/("""
        padding: 9px 0;
      """),format.raw/*14.7*/("""}"""),format.raw/*14.8*/("""

      @media (max-width: 980px) """),format.raw/*16.34*/("""{"""),format.raw/*16.35*/("""
        /* Enable use of floated navbar text */
        .navbar-text.pull-right """),format.raw/*18.33*/("""{"""),format.raw/*18.34*/("""
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        """),format.raw/*22.9*/("""}"""),format.raw/*22.10*/("""
      """),format.raw/*23.7*/("""}"""),format.raw/*23.8*/("""
    </style>
  <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*25.48*/routes/*25.54*/.Assets.at("stylesheets/bootstrap-responsive.css"))),format.raw/*25.104*/("""">
  <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*26.48*/routes/*26.54*/.Assets.at("stylesheets/font-awesome.min.css"))),format.raw/*26.100*/("""">
  <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*27.48*/routes/*27.54*/.Assets.at("stylesheets/app.css"))),format.raw/*27.87*/("""">
  <!--[if lt IE 9]>
  <script src=""""),_display_(Seq[Any](/*29.17*/routes/*29.23*/.Assets.at("javascripts/html5shiv.js"))),format.raw/*29.61*/("""" type="text/javascript"></script>
  <![endif]-->
  <!--[if IE 7]>
  <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*32.48*/routes/*32.54*/.Assets.at("stylesheets/font-awesome-ie7.min.css"))),format.raw/*32.104*/("""">
  <![endif]-->
  <!-- Le styles -->
</head>
<body>
    <div id="app-container">
        <a href="#start">Start Application</a>
    </div>
    <div id="container">
    </div>
    <script src=""""),_display_(Seq[Any](/*42.19*/routes/*42.25*/.Assets.at("javascripts/app.js"))),format.raw/*42.57*/("""" type="text/javascript"></script>
</body>
</html>"""))}
    }
    
    def render(): play.api.templates.Html = apply()
    
    def f:(() => play.api.templates.Html) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 31 12:05:41 CST 2013
                    SOURCE: F:/play-backbone-master/app/views/index.scala.html
                    HASH: d63bf9e98dce5ca18bff2f185fb40fdf501ad595
                    MATRIX: 787->0|973->151|987->157|1047->196|1114->236|1142->237|1233->301|1261->302|1309->322|1338->323|1396->354|1424->355|1486->390|1515->391|1624->472|1653->473|1771->564|1800->565|1834->572|1862->573|1959->634|1974->640|2047->690|2133->740|2148->746|2217->792|2303->842|2318->848|2373->881|2448->920|2463->926|2523->964|2673->1078|2688->1084|2761->1134|2992->1329|3007->1335|3061->1367
                    LINES: 29->1|34->6|34->6|34->6|36->8|36->8|39->11|39->11|40->12|40->12|42->14|42->14|44->16|44->16|46->18|46->18|50->22|50->22|51->23|51->23|53->25|53->25|53->25|54->26|54->26|54->26|55->27|55->27|55->27|57->29|57->29|57->29|60->32|60->32|60->32|70->42|70->42|70->42
                    -- GENERATED --
                */
            