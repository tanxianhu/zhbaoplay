// @SOURCE:F:/play-backbone-master/conf/routes
// @HASH:96dcae53231f3a5ec3864b8271b4e2ce21253854
// @DATE:Mon Jul 29 17:09:54 CST 2013

package controllers;

public class routes {
public static final controllers.ReverseUserServices UserServices = new controllers.ReverseUserServices();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static class javascript {
public static final controllers.javascript.ReverseUserServices UserServices = new controllers.javascript.ReverseUserServices();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();    
}   
public static class ref {
public static final controllers.ref.ReverseUserServices UserServices = new controllers.ref.ReverseUserServices();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();    
} 
}
              