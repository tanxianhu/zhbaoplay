// @SOURCE:F:/play-backbone-master/conf/routes
// @HASH:96dcae53231f3a5ec3864b8271b4e2ce21253854
// @DATE:Mon Jul 29 17:09:54 CST 2013


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix  
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" } 


// @LINE:6
private[this] lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:7
private[this] lazy val controllers_UserServices_getUser1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("service/users/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:8
private[this] lazy val controllers_UserServices_saveUser2 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("service/users/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:11
private[this] lazy val controllers_Assets_at3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """service/users/$id<[^/]+>""","""controllers.UserServices.getUser(id:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """service/users/$id<[^/]+>""","""controllers.UserServices.saveUser(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
       
    
def routes:PartialFunction[RequestHeader,Handler] = {        

// @LINE:6
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:7
case controllers_UserServices_getUser1(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.UserServices.getUser(id), HandlerDef(this, "controllers.UserServices", "getUser", Seq(classOf[String]),"GET", """""", Routes.prefix + """service/users/$id<[^/]+>"""))
   }
}
        

// @LINE:8
case controllers_UserServices_saveUser2(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(controllers.UserServices.saveUser(id), HandlerDef(this, "controllers.UserServices", "saveUser", Seq(classOf[String]),"PUT", """""", Routes.prefix + """service/users/$id<[^/]+>"""))
   }
}
        

// @LINE:11
case controllers_Assets_at3(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}
    
}
        