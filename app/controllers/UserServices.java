package controllers;



import play.mvc.*;

import static play.libs.Json.toJson;
import org.codehaus.jackson.JsonNode;

import play.mvc.BodyParser;

import me.ezcode.pojo.User;

public class UserServices extends Controller {
  
    public static Result getUser(String userId) {
        User user = User.USERS.get(userId);
        return ok(toJson(user));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result saveUser(String userId){
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").getTextValue();
        User user = User.USERS.get(userId);
        user.setName(name);

        return ok(toJson(user));
    }
}
