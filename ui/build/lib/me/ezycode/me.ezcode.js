(function() {
  var Backbone, Collection, Model, Q, Router, View, backbone_sync, bb, d, _ref, _ref1, _ref2, _ref3,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __slice = [].slice;

  require("jquery");

  bb = require("backbone");

  d = require("dustjs-linkedin");

  require("dustjs-linkedin-helpers");

  Q = require('q');

  backbone_sync = bb.sync;

  bb.sync = function(method, model, options) {
    return Q.when(backbone_sync(method, model, options));
  };

  Collection = (function(_super) {
    __extends(Collection, _super);

    function Collection() {
      _ref = Collection.__super__.constructor.apply(this, arguments);
      return _ref;
    }

    Collection.prototype.fetch = function() {
      var options,
        _this = this;
      options = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      return Collection.__super__.fetch.apply(this, options).then(function() {
        return _this;
      });
    };

    return Collection;

  })(bb.Collection);

  View = (function(_super) {
    __extends(View, _super);

    function View() {
      _ref1 = View.__super__.constructor.apply(this, arguments);
      return _ref1;
    }

    View.prototype.q = Q;

    View.prototype.renderDust = function(name, context) {
      var deferred,
        _this = this;
      deferred = Q.defer();
      d.render(name, context, function(err, out) {
        if (err != null) {
          return deferred.reject(err);
        } else {
          return deferred.resolve(out);
        }
      });
      return deferred.promise;
    };

    View.prototype.release = function() {
      return stopListening();
    };

    return View;

  })(bb.View);

  Model = (function(_super) {
    __extends(Model, _super);

    function Model() {
      _ref2 = Model.__super__.constructor.apply(this, arguments);
      return _ref2;
    }

    return Model;

  })(bb.Model);

  Router = (function(_super) {
    __extends(Router, _super);

    function Router() {
      _ref3 = Router.__super__.constructor.apply(this, arguments);
      return _ref3;
    }

    return Router;

  })(bb.Router);

  Backbone = {
    Collection: Collection,
    View: View,
    Model: Model,
    Router: Router,
    history: bb.history
  };

  module.exports.Backbone = Backbone;

}).call(this);
