require.config
　　　　paths: 
　　　　　　"GDmap":"http://webapi.amap.com/maps?v=1.2&key=45b9bf3eb054e1620fe443491d893684"

Backbone = require('me.ezcode').Backbone
require("jquery")
AMap=require("GDmap")
###
User = require('./models/user.coffee')
UserEditor = require('./views/user-editor.coffee')
###
class App extends Backbone.Router
    routes:
        "":         "home"
        "start":    "startApp"

    home: ->

    startApp: ->
        ###
        user = new User
            id: "0001"
        userEditor = new UserEditor
            user: user
        user.fetch()
        $("#app-container").html(userEditor.el)
        ###
        position=new AMap.LngLat(108.903560,34.23090)
        mapObj=new AMap.Map("container",{center:position})
        marker = new AMap.Marker
           map:mapObj                 
           position:position   
           icon:"http://webapi.amap.com/images/1.png"
     	   zoom:8
        

$ ->
    app = new App()
    Backbone.history.stop()
    Backbone.history.start()